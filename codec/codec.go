package codec

import (
	"gitlab.com/ouqiang/socket/message"
)

// Codec 编解码器
type Codec interface {
	// Marshal 编码
	Marshal(msgType int, payload interface{}) ([]byte, error)
	// Unmarshal 解码
	Unmarshal(data []byte, registry *message.Registry) (msgType int, payload interface{}, err error)
	// String 名称
	String() string
}
