module gitlab.com/ouqiang/socket

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gogo/protobuf v1.2.0
	github.com/gorilla/websocket v1.4.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/posener/wstest v0.0.0-20180217133618-28272a7ea048
	github.com/stretchr/testify v1.2.2
)
